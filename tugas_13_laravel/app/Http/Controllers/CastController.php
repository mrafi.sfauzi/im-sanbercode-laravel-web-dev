<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title='Cast';
        $dataCast=DB::table('cast')->get();
        return view('pages.cast.home',[
            'title'=>$title,
            'datas'=>$dataCast
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title='Cast';
        $dataCast=DB::table('cast')->get();
        return view('pages.cast.create',[
            'title'=>$title,
            'datas'=>$dataCast
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData= $request->validate([
            'nama' => 'required|min:3',
            'umur' => 'required|numeric|min:10',
            'bio' => 'required|min:10',         
        ]);
        
        $validateData['nama']=ucwords($request->nama);
        $input=DB::table('cast')->insert($validateData);
        if ($input) {
            return redirect('/cast')->with('success','Data Berhasil Ditambahkan');
        }else{
            return redirect('/cast')->with('error','Data Gagal Ditambahkan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $title='View Cast';
        $dataCast=DB::table('cast')->where('id','=',$id)->first();
        return view('pages.cast.show',[
            'title'=>$title,
            'data'=>$dataCast
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title='Edit Cast';
        $dataCast=DB::table('cast')->where('id','=',$id)->first();
        return view('pages.cast.edit',[
            'title'=>$title,
            'id'=>$id,
            'data'=>$dataCast
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validateData= $request->validate([
            'nama' => 'required|min:3',
            'umur' => 'required|numeric|min:10',
            'bio' => 'required|min:10',         
        ]);
        
        $validateData['nama']=ucwords($request->nama);
        $input=DB::table('cast')->where('id','=',$id)->update($validateData);
        if ($input) {
            return redirect('/cast')->with('success','Data Berhasil Diubah.');
        }else{
            return redirect('/cast')->with('error','Data Gagal Diubah / Data Tidak Ada Yang Diubah.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete=DB::table('cast')->where('id','=',$id)->delete();
        if ($delete) {
            return redirect('/cast')->with('success','Data Berhasil Dihapus.');
        }else{
            return redirect('/cast')->with('error','Data Gagal Dihapus.');
        }
    }
}
