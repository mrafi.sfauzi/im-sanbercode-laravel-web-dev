<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CastController;
use App\Http\Controllers\DaftarController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[DaftarController::class,'index']);
Route::get('/dashboard',[DaftarController::class,'index']);
Route::get('/daftar',[DaftarController::class,'create']);
Route::post('/kirim',[DaftarController::class,'store']);

// cast
Route::get('/cast',[CastController::class,'index']);
Route::get('/cast/create',[CastController::class,'create']);
Route::post('/cast',[CastController::class,'store']);
Route::get('/cast/{cast_id}',[CastController::class,'show']);
Route::get('/cast/{cast_id}/edit',[CastController::class,'edit']);
Route::put('/cast/{cast_id}',[CastController::class,'update']);
Route::delete('/cast/{cast_id}',[CastController::class,'destroy']);

Route::get('/table/data-tables', function () {
    return view('pages.table',['title'=>'Table']);
});
Route::get('/data-tables', function () {
    return view('pages.dataTable',['title'=>'Data Table']);
});

