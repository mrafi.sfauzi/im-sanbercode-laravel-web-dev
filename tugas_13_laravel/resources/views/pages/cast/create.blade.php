@extends('template.main')

@section('title')
    <div class="d-flex justify-content-between mb-3 p-2">
        <h1>Tambah Cast</h1>
        <a href="/cast" class="btn btn-sm btn-primary">List Data</a>
    </div>
@endsection


@section('container')
    <div class="row">
        <div class="col-lg-6">
            <form action="/cast" method="post">
            @csrf
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="nama">Nama</label>
                        <input type="text" name="nama" class="form-control rounded" value='{{ old('nama') }}' id="nama" placeholder="Nama Lengkap" required>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="umur">Umur</label>
                        <input type="number" name="umur" class="form-control rounded @if ($errors->has('umur')) border-danger @endif" id="umur" value='{{ old('umur') }}' placeholder="Umur Minimal 10 Tahun" required>
                        @if ($errors->has('umur')) <span class="text-danger">{{ $errors->first('umur') }}</span> @endif
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="Biodata">Biodata</label> <small id="countChar"></small>
                <textarea name="bio" id="bio" class="form-control rounded @if ($errors->has('bio')) border-danger @endif" cols="30" rows="10" maxlength="200" placeholder="Biodata Lengkap Maksimal 200 Karakter" required>@if($errors->all()){{old('bio')}}@endif</textarea>
                @if ($errors->has('bio')) <span class="text-danger">{{ $errors->first('bio') }}</span> @endif
            </div>
            <div class="text-left">
                <button type="submit" class="btn btn-success mr-2">Simpan</button>
                <button type="reset" class="btn btn-danger">Reset</button>
            </div>
            </form>
        </div>
    </div>
@endsection
@push('script')
<script>
    var textarea=document.getElementById("bio")
    textarea.addEventListener("keyup", function () {
        var value=textarea.value;
        var length=value.length;
        var max = 200;

        if (length>max) {
            alert('sudah melebihi maksimum');
        }else{
            document.getElementById('countChar').innerHTML=max-length+' karakter tersisa';
        }
        
    });
</script>
@endpush
