@extends('template.main')

@section('title')
<div class="d-flex justify-content-between mb-3 p-2">
    <h1>View Cast</h1>
    <a href="/cast" class="btn btn-sm btn-primary">List Data</a>
</div>
@endsection


@section('container')
    <div class="row">
        <div class="col-lg-6">
            <h2 class="display-4">{{ $data->nama }}</h2>
            <h6>{{ $data->umur }} Tahun</h6>
            <p>{{ $data->bio }}</p>
        </div>
    </div>
    <a href="/cast" class="btn btn-sm btn-info">Kembali</a>
@endsection
@push('script')
@endpush
