@extends('template.main')

@section('title')
    <div class="d-flex justify-content-between mb-3 p-2">
        <h1>List Cast</h1>
        <a href="/cast/create" class="btn btn-sm btn-primary">Tambah Data</a>
    </div>
@endsection


@section('container')
    <div id="alert">
        @if (session('success'))
            <div class="alert alert-success">{{ session('success') }}</div>
        @endif
        @if (session('error'))
            <div class="alert alert-danger">{{ session('error') }}</div>
        @endif
    </div>
    <table id="example1" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>Nama</th>
                <th>Umur</th>
                <th>Bio</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($datas as $data)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $data->nama }}</td>
                    <td>{{ $data->umur }}</td>
                    <td>{{ $data->bio }}</td>
                    <td>
                        <button type="button" class="btn btn-sm btn-info" data-toggle="dropdown">
                            Aksi
                        </button>
                        <div class="dropdown-menu" role="menu">
                            <a class="dropdown-item text-white" href="/cast/{{ $data->id }}">Show</a>
                            <a class="dropdown-item text-white" href="/cast/{{ $data->id }}/edit">Edit</a>
                            <form action="/cast/{{ $data->id }}" method="post">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="dropdown-item">Delete</button>
                            </form>
                        </div>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
@push('script')
    <script type="text/javascript">
        $(document).ready(function() {
            setTimeout(function() {
                $('#alert').fadeOut(function() {
                    $('#alert').remove();
                });
            }, 2000);
        });
    </script>
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
    <script src="plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
    <script src="plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
    <script src="plugins/datatables-buttons/js/buttons.html5.min.js"></script>
    <script src="plugins/datatables-buttons/js/buttons.print.min.js"></script>
    <script src="plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
    <script>
        $(function() {
            $('#example1').DataTable({
                columnDefs: [
                { targets: [3,4], orderable: false } // Disable sorting for columns 0 and 2
                ]
            });
        });
    </script>
@endpush
