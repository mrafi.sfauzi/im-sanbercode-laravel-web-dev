@extends('template.main')

@section('title')
    <h1>Sign Up</h1>
@endsection

@section('container')
    <form action="/kirim" method="post">
        @csrf
        <br>First name:
        <br><input type="text" class="mb-3"  name="first_name" id="first_name">
        <br>Last name:
        <br><input type="text" class="mb-3" name="last_name" id="last_name">
        <br>
        <button type="submit" class="mb-3 btn btn-primary" onclick="get()">Sign Up</button>
    </form>
    @if (count($errors) > 0)
        <hr>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif
@endsection
