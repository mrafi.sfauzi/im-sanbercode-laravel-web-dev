<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>

    <form action="/welcome" method="post">
        @csrf
        <p>First name:</p>
        <input type="text" name="first_name" id="first_name">
        <p>Last name:</p>
        <input type="text" name="last_name" id="last_name">
        <p>Gender:</p>
        <input type="radio" name="gender" id="gender" value="male"> Male <br>
        <input type="radio" name="gender" id="gender" value="female"> Female <br>
        <input type="radio" name="gender" id="gender" value="other"> Other <br>
        <p>Nationality:</p>
        <select name="nationality" id="nationality">
            <option value="indonesian">Indonesian</option>
            <option value="malaysian">Malaysian</option>
            <option value="singaporean">Singaporean</option>
            <option value="australian">Australian</option>
        </select>
        <p>Language Spoken:</p>
        <input type="checkbox" name="language" id="language" value="ID"> Bahasa Indonesia <br>
        <input type="checkbox" name="language" id="language" value="EN"> English <br>
        <input type="checkbox" name="language" id="language" value="other"> Other <br>
        <p>Bio:</p>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
        <p>
            <button type="submit" onclick="get()">Sign Up</button>
    </form>
    @if (count($errors) > 0)
        <hr>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif
</body>

</html>
