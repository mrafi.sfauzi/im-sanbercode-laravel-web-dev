<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('register');
    }

    public function store(Request $request)
    {
        $validateData= $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'gender' => 'required',
            'nationality' => 'required',
            'language' => 'required',
            'bio' => 'required',           
        ]);

        $firsName=$validateData['first_name'];
        $lastName=$validateData['last_name'];
        $name=ucwords($firsName.' '.$lastName);
        return view('welcome',[
            'name'=>$name
        ]);
        
    }

    
}
