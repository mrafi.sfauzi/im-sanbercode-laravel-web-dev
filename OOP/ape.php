<?php
require_once('animal.php');

class Ape extends Animal
{
    public $legs=12;
    public $cold_blooded='no';

    function yell()
    {
        return 'AUOOOOOOOO';
    }
}
?>