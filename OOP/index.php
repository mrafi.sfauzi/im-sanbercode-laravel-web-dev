<?php 
    require_once ('animal.php');
    require ('frog.php');
    require ('ape.php');

    

$sheep = new Animal("shaun");
echo "Name : ".$sheep->name."<br>"; // "shaun"
echo "Legs : ".$sheep->legs."<br>"; // 4
echo "Cold Blooded : ".$sheep->cold_blooded."<br>"; // "no"

echo "<br><br>";

$frog = new Frog("buduk");
echo "Name : ".$frog->name."<br>"; // "shaun"
echo "Legs : ".$frog->legs."<br>"; // 4
echo "Cold Blooded : ".$frog->cold_blooded."<br>"; // "no"
echo "Jump : ".$frog->jump()."<br>";

echo "<br><br>";

$ape = new Ape("kera sakti");
echo "Name : ".$ape->name."<br>"; // "shaun"
echo "Legs : ".$ape->legs."<br>"; // 4
echo "Cold Blooded : ".$ape->cold_blooded."<br>"; // "no"
echo "Yell : ".$ape->yell()."<br>";
?>