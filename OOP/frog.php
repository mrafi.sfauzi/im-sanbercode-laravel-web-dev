<?php
require_once('animal.php');

class Frog extends Animal
{
    public $legs=2;
    public $cold_blooded='no';

    function jump()
    {
        return 'hop hop';
    }

}
?>